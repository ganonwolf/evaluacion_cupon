<?php

class DataBase{
	private $db;

	public function __construct($ip,$dbname,$user,$pass,$port=3306){
		$this->db=new PDO("mysql:host={$ip}".($port!=3306?":{$port}":"").";dbname={$dbname};charset=utf8mb4",$user,$pass);
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		return $this->db;
	}	

	public function pdo(){
		return $this->db;
	}


}