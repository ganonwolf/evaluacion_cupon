<?php

class Email{
	private $from;
	private $to;
	private $subject;
	private $message;
	private $headers;
	private $cc;

	public function setFrom($f,$name){
		$this->from=$f;
	}

	public function setTo($t,$name){
		$this->to[]=$t;
	}

	public function setcc($cc){
		$this->cc[]=$cc;
	}

	public function setSubject($s){
		$this->subject=$s;
	}

	public function setMessage($m){
		$this->message=$m;
	}

	public function setHeader(){
		$this->headers  = "MIME-Version: 1.0" . "\r\n";
		$this->headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";

		// Cabeceras adicionales
		//$this->headers .= "To: ".implode(',',$this->to). "\r\n";
		$this->headers .= "From: {$this->from}" . "\r\n";
		$this->headers .= "Reply-to: {$this->from}" . "\r\n";
		$this->headers .= "Cc: ".implode(',',$this->cc) . "\r\n";
	}

	public function sendMail(){
		return mail(implode(',',$this->to), $this->subject, $this->message, $this->headers);
	}
	
}