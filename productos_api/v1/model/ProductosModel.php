<?php
class ProductosModel{
	public $titulo;
	public $img_path;
	public $descripcion;
	public $tipo;
	public $nombre_tipo;
	public $fe_ini;
	public $fe_fin;
	public $rut_banc;
	public $nombre_banc;
	public $email_banc;
	public $num_banc;
	public $tipo_banc;
	public $id_banco;
	public $banco;
	public $beneficiarios;
	public $clave;
	public $msj_return;
	public $comuna  ;   
    public $id_comuna  ;
    public $region  ;   
    public $id_region  ;
    public $monto_teorico;
    public $monto_total;
    public $ult_date_coop;
    public $cod_area;

	public function __construct(){

    }

    public function setCodArea($cod){
        $this->cod_area=$cod;
    }

    public function getCodArea(){
        return $this->cod_area;
    }

    public function setRegion($region){
        $this->region=$region;
    }

    public function getRegion(){
        return $this->region;
    }

    public function setIdRegion($id){
    	$this->id_region=$id;
    }

    public function getIdRegion(){
    	return $this->id_region;
    }

    public function setComuna($comuna){
        $this->comuna=$comuna;
    }

    public function getComuna(){
        return $this->comuna;
    }

    public function setIdComuna($id){
    	$this->id_comuna=$id;
    }

    public function getIdComuna(){
    	return $this->id_comuna;
    }

    public function setReturnMsj($msj){
    	$this->msj_return=$msj;
    }

    public function getReturnMsj(){
    	return $this->msj_return;
    }

    public function setClave($clave){
    	$this->clave=$clave;
    }

    public function getClave(){
    	return $this->clave;
    }

    public function setBeneficiarios($ben){
    	$this->beneficiarios=$ben;
    }

    public function getBeneficiarios(){
    	return $this->beneficiarios;
    }

    public function setBanco($b){
    	$this->banco=$b;
    }

    public function getBanco(){
    	return $this->banco;
    }

    public function setIdBanco($id){
    	$this->id_banco=$id;
    }

    public function getIdBanco(){
    	return $this->id_banco;
    }

    public function setTipoBanco($tb){
    	$this->tipo_banc=$tb;
    }

    public function getTipoBanco(){
    	return $this->tipo_banc;
    }

    public function setNumeroCuenta($num){
    	$this->num_banc=$num;
    }

    public function getNumeroCuenta(){
    	return $this->num_banc;
    }

    public function setEmailCliente($email){
    	$this->email_banc=$email;
    }

    public function getEmailCliente(){
    	return $this->email_banc;
    }

    public function setNombreCliente($nbanco){
    	$this->nombre_banc=$nbanco;
    }

    public function getNombreCliente(){
    	return $this->nombre_banc;
    }

    public function setRutBanco($rut){
    	$this->rut_banc=$rut;
    }

    public function getRutBanco(){
    	return $this->rut_banc;
    }

    public function setTitulo($titulo){
    	$this->titulo=$titulo;
    }

    public function getTitulo(){
    	return $this->titulo;
    }

    public function setImg($img){
    	$this->img_path=$img;
    }

    public function getImg(){
    	return $this->img_path;
    }
    public function setDescripcion($desc){
    	$this->descripcion=$desc;
    }

    public function getDescripcion(){
    	return $this->descripcion;
    }
    public function setTipo($tipo){
    	$this->tipo=$tipo;
    }

    public function getTipo(){
    	return $this->tipo;
    }
    public function setTipoNombre($nombre){
    	$this->nombre_tipo=$nombre;
    }

    public function getTipoNombre(){
    	return $this->nombre_tipo;
    }
    public function setFechaInicio($fecha){
    	$this->fe_ini=$fecha;
    }

    public function getFechaInicio(){
    	return $this->fe_ini;
    }
   public  function setFechaTermino($fecha){
    	$this->fe_fin=$fecha;
    }

    public function getFechaTermino(){
    	return $this->fe_fin;
    }

}