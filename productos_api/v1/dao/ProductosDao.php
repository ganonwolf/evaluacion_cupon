<?php

class ProductosDao{
	private $dbpdo = NULL;
 	private $data=null;
    public function __construct($param){
//        parent::__construct();
        $this->data=$param;              // Init parent contructor
        $this->dbConnect();                 // Initiate Database connection
    }
     
    private function dbConnect(){
        $this->dbpdo=new DataBase(DB_IP,DB_NAME,DB_USER,DB_PSS,DB_PORT);
    }

    public function findProducto(){
    	
    	$query="Select Titulo, Descripcion, FechaInicio, FechaTermino, Precio, Imagen from ".DB_NAME.".productos where 1=1  ";

        if($this->data->keyword!=""){
            $query.=" and Titulo like :busqueda";
        }
        //print_r($this->data);
        if($this->data->page>=0&&$this->data->cant>0){
            $query.=" limit :page , :cant";
        }
    	$stmt=$this->dbpdo->pdo()->prepare($query);
    	$stmt->setFetchMode(PDO::FETCH_ASSOC);
        if($this->data->keyword!=""){
            $value=$this->data->keyword.'%';
    	   $stmt->bindParam(':busqueda', $value);
    	}

        if($this->data->page>=0&&$this->data->cant>0){
    
            $stmt->bindParam(':page', $this->data->page);
            $stmt->bindParam(':cant', $this->data->cant);
        }
        $stmt->execute();
    	$rsObjt=array();

    	while($row = $stmt->fetch()){
            $row["FechaInicio"]=date("d/m/Y",strtotime($row["FechaInicio"]));
            $row["FechaTermino"]=date("d/m/Y",strtotime($row["FechaTermino"]));
        	$rsObjt[]=$row;
            
    	}

        $query="Select count(*) as total from ".DB_NAME.".productos where 1=1  ";

        $stmt=$this->dbpdo->pdo()->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute();
        $row = $stmt->fetch();
    	
    	return array("data"=>$rsObjt,"count"=>$row["total"]);
    }

    public function listarEstadistica(){
        $query="Select Titulo, Descripcion, Precio, Imagen, Vendidos, Tags from ".DB_NAME.".productos where 1=1  ";

        $query.=" order by Vendidos desc limit 20";

        $stmt=$this->dbpdo->pdo()->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute();
        $rsObjt=array();
   
        while($row = $stmt->fetch()){
            $tags=explode(",",$row["Tags"]);
            $firsFive=array();
            for($i=0;$i<5;$i++){
                if(isset($tags[$i])){
                    $firsFive[]=$tags[$i];
                }
            }
            $row["Tags"]=implode(", ",$firsFive);
            $rsObjt[]=$row;

        }
        return $rsObjt;

    }

}