<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL);

//Paths structure
define("HOME_PATH",__DIR__);
define("MODEL",HOME_PATH."/model/");
define("CONTROLLERS",HOME_PATH."/controller/");
define("DAO",HOME_PATH."/dao/");

//libs
define("RESTFUL",HOME_PATH."/RestFul/REST.php");
define("DATABASE",HOME_PATH."/libs/DataBase.php");

//autoloads
//composer
define("AUTOLOAD_COMPOSER",HOME_PATH."/../vendor/autoload.php");

//custom autoload
define("AUTOLOAD_MODELS",MODEL."autoload.php");
define("AUTOLOAD_CONTROLLERS",CONTROLLERS."autoload.php");
define("AUTOLOAD_DAO",DAO."autoload.php");

//resource
define("IMG_PATH",HOME_PATH."/../resource/img/FotosEventos/");
define("IMG_PATH_ROOT",HOME_PATH."/../resource/img/");

//config db, en ambiente desa y produccion
define("DB_IP", "192.168.10.10"); //<-- apuntar ip hacia el host de la base de datos
define("DB_PORT","3306");//<-- puerto que utiliza

define("DB_USER","homestead"); //<-- nombre del usuario de la base de datos
define("DB_PSS","secret");//<-- contraseña del usuario de la base de datos
define("DB_NAME","test_productos");//<-- nombre de la base de datos
