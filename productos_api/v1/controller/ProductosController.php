<?php

class ProductosController{
	private $params;
	public function __construct($data){
		$this->params=$data;
    }


    public function searchAction(){
        $evntDao=new ProductosDao(json_decode($this->params));
        return $evntDao->findProducto();
    }

    public function estadisticasAction(){
    	$evntDao=new ProductosDao(json_decode($this->params));
    	return $evntDao->listarEstadistica();
    }

    
}