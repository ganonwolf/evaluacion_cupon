<?php
require_once("config.php");

require_once(RESTFUL);
require_once(DATABASE);
//require_once(AUTOLOAD_COMPOSER);
require_once(AUTOLOAD_MODELS);
require_once(AUTOLOAD_DAO);
require_once(AUTOLOAD_CONTROLLERS);
     
class API extends REST {
     
    public $data = "";
    
    public function __construct(){
        parent::__construct();              // Init parent contructor
                       
    }
     
     
    /*
     * Public method for access api.
     * This method dynmically call the method based on the query string
     *
     */
    public function processApi(){
        $class = strtolower(trim(str_replace("/","",$_REQUEST['ccpp'])));
        $func = trim(str_replace("/","",$_REQUEST['pp']))."Action";
            
        $objClass=null;

        switch ($class) {
            case "productos":
                $objClass = new ProductosController($this->_request);    
                break;
            default:
                # code...
                break;
        }

        if((int)method_exists($objClass,$func) > 0){
            $data=null;
            switch ($this->get_request_method()) {
                case "GET":
                    foreach ($objClass->$func() as $object){$data[] = (array) $object;}
                    $this->response($this->json($data), 200); 
                    break;
                case "POST":
                    foreach ($objClass->$func() as $object){$data[] = (array) $object;}
                    $this->response($this->json($data), 200); 
                    break;
               
                /*case "OPTIONS":
                    foreach ($objClass->$func() as $object){$data[] = (array) $object;}
                    $this->response($this->json($data), 200); 
                    break;*/
                default:
                    $this->response('Error code 404, Page not found 2',404);
                    break;
            } 
       } else{
            $this->response('Error code 404, Page not found 88'.$class,404);   // If the method not exist with in this class, response would be "Page not found".
       }
        
    }     
    /*
     *  Encode array into JSON
    */
    private function json($data){
        if(is_array($data)){
            return json_encode($data);
        }
    }
}
 
    // Initiiate Library
     
    $api = new API;
    $api->processApi();
