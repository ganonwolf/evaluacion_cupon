# Evaluacion Proyecto

Este repositorio incluye servicios REST (PHP), front (html, js) y base de datos.

### Instalacion

Primero: Debe descargar repositorio

Segundo: Debe copiar en la raiz html el servicio REST que se ubica en la carpeta productos_api (ese necesario la ubicacion en la raiz html, ya que la constantes en front end se define como: "http://localhost:80/productos_api/v1")

Tercero: Copiar en raiz html el contenido de la carpeta front_end

Cuarto: Ejecutar query del archivo .sql ubicado en la carpeta data_base

Quinto: Configurar variables constantes en archivo php del servicio, ubicado en productos_api/v1/config.php

define("DB_IP", "192.168.10.10"); //<-- apuntar ip hacia el host de la base de datos

define("DB_PORT","3306");//<-- puerto que utiliza

define("DB_USER","homestead"); //<-- nombre del usuario de la base de datos

define("DB_PSS","secret");//<-- contraseña del usuario de la base de datos

define("DB_NAME","test_productos");//<-- nombre de la base de datos


## Autores

* **Johanm Fernandez** - *Desarrolador* -


